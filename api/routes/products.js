const express = require("express");
const router = express.Router();

const ProductSchema = require("../models/product-model");
const mongoose = require("mongoose");

router.get("/getAllProducts", (req, res, next) => {
  console.log("inside get");
  ProductSchema.find()
    .then(result => {
      res.status(200).json({
        body: result
      });
    })
    .catch(error => {
      res.status(500).json({
        message: error
      });
    });
});

// ***********************
router.get("/:id", (req, res, next) => {
  console.log("inside get id");
  var id = req.params.id;
  var query = { productId: id };
  ProductSchema.find(query)
    .then(result => {
      res.status(200).json({
        body: result
      });
    })
    .catch(error => {
      res.status(500).json({
        message: error
      });
    });
});

// ***********************
router.post("/createProduct", (req, res, next) => {
  console.log("inside post");
  console.log(req);
  const newproduct = new ProductSchema({
    _id: new mongoose.Types.ObjectId(),
    productId: req.body.productId,
    size: req.body.size,
    brandName: req.body.brandName
  });
  newproduct
    .save()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: result
      });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({
        message: error
      });
    });
  var id = req.body;
});

module.exports = router;
