const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  _id: mongoose.SchemaTypes.ObjectId,
  productId: String,
  size: String,
  brandName: String
});

module.exports = mongoose.model("ProductSchema", productSchema);
