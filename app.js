const express = require("express");
const app = express();

const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const products = require("./api/routes/products");

mongoose.connect("mongodb://localhost/mini-amazon");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/products", products);

module.exports = app;
